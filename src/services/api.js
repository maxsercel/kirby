import axios from "axios";

const tmdbUrl = "https://api.themoviedb.org/3";
const tmdbKey = process.env.REACT_APP_TMDB_KEY;

const axiosClient = axios.create({ baseURL: tmdbUrl });

export const getPopularMovies = async (selectedPage = 1) => {
  try {
    const { data } = await axiosClient({
      method: "get",
      url: "/movie/popular",
      params: {
        api_key: tmdbKey,
        language: "en-US",
        page: selectedPage,
      },
    });
    return data;
  } catch (error) {
    return error.message;
  }
};

export const getPopularTVShows = async (selectedPage = 1) => {
  try {
    const { data } = await axiosClient({
      method: "get",
      url: "/tv/popular",
      params: {
        api_key: tmdbKey,
        language: "en-US",
        page: selectedPage,
      },
    });
    return data;
  } catch (error) {
    return error.message;
  }
};

export const getSearchedMovieAndTV = async (query, selectedPage = 1) => {
  try {
    const moviePromise = axiosClient({
      method: "get",
      url: `/search/movie`,
      params: {
        query: query,
        api_key: tmdbKey,
        language: "en-US",
        page: selectedPage,
      },
    });

    const tvPromise = axiosClient({
      method: "get",
      url: `/search/tv`,
      params: {
        query: query,
        api_key: tmdbKey,
        language: "en-US",
        page: selectedPage,
      },
    });

    const [movieData, tvData] = await Promise.all([moviePromise, tvPromise]);
    return {
      results: [...movieData.data.results, ...tvData.data.results],
      total_pages: Math.max(movieData.data.total_pages, tvData.data.total_pages),
      total_results: movieData.data.total_results + tvData.data.total_results,
    };
  } catch (error) {
    return error.message;
  }
};

export const getSingleItem = async (mediaType, id) => {
  try {
    const { data } = await axiosClient({
      method: "get",
      url: `/${mediaType}/${id}`,
      params: {
        api_key: tmdbKey,
        language: "en-US",
      },
    });
    return data;
  } catch (error) {
    return error.message;
  }
};

export const getSingleItemCredits = async (mediaType, id) => {
  try {
    const { data } = await axiosClient({
      method: "get",
      url: `/${mediaType}/${id}/credits`,
      params: {
        api_key: tmdbKey,
        language: "en-US",
      },
    });
    return data;
  } catch (error) {
    return error.message;
  }
};

export const getSimilarItems = async (mediaType, id) => {
  try {
    const { data } = await axiosClient({
      method: "get",
      url: `/${mediaType}/${id}/similar`,
      params: {
        api_key: tmdbKey,
        language: "en-US",
      },
    });
    return data;
  } catch (error) {
    return error.message;
  }
};
