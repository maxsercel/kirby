import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {
  MaxWidthLayout,
  NavbarFooterIncluded,
  OtherSection,
  TopSection,
  MovieContainer,
} from "layouts";
import {
  getSingleItem,
  getSingleItemCredits,
  getSimilarItems,
} from "services/api";
import { truncateString } from "utils/truncateString";
import { extractImgPoster } from "utils/extractImg";
import { BsStarFill } from "react-icons/bs";

const SingleMovie = () => {
  const [item, setItem] = useState();
  const [credits, setCredits] = useState();
  const [similarItems, setSimilarItems] = useState();
  const [server, setServer] = useState(1);
  const [season, setSeason] = useState(1);
  const [episode, setEpisode] = useState(1);
  const { mediaType, id } = useParams();

  useEffect(() => {
    (async function () {
      const result = await getSingleItem(mediaType, id);
      const creditResult = await getSingleItemCredits(mediaType, id);
      const { results: similarItemsResult } = await getSimilarItems(mediaType, id);
      setItem(result);
      setCredits(creditResult);
      setSimilarItems(similarItemsResult.slice(0, 10));
    })();
  }, [mediaType, id]);

  if (!item) return null;

  const videoSrc = () => {
    if (mediaType === 'movie') {
      switch (server) {
        case 1: return `https://vidsrc.to/embed/movie/${id}`;
        case 2: return `https://moviesapi.club/movie/${id}`;
        case 3: return `https://multiembed.mov/?video_id=${id}&tmdb=1`;
        default: return `https://vidsrc.to/embed/movie/${id}`;
      }
    } else {
      switch (server) {
        case 1: return `https://vidsrc.to/embed/tv/${id}/${season}/${episode}`;
        case 2: return `https://moviesapi.club/tv/${id}-${season}-${episode}`;
        case 3: return `https://multiembed.mov/?video_id=${id}&tmdb=1&s=${season}&e=${episode}`;
        default: return `https://vidsrc.to/embed/tv/${id}/${season}/${episode}`;
      }
    }
  };

  return (
    <NavbarFooterIncluded>
      <div
        style={{
          backgroundImage: `linear-gradient(180deg, rgba(0,0,0,0.9) 0%, rgba(0,0,0,0.7) 50%, rgba(0,0,0,0.9) 100%), url(https://image.tmdb.org/t/p/original/${item.backdrop_path})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          backgroundPosition: "top center",
        }}
        className="min-h-[60vh] w-full flex items-center py-28 md:py-36 space-y-10"
      >
        <MaxWidthLayout>
          <div className="grid grid-cols-1 md:grid-cols-[28%_70%] gap-[2%]">
            <div className="flex items-center justify-center md:justify-start">
              <img
                src={extractImgPoster(item.poster_path)}
                alt={item.title || item.name}
                className="rounded-md shadow-lg"
              />
            </div>
            <div className="self-center space-y-5">
              <h1 className="text-center md:text-left custom-movie-title">
                {item.title || item.name}
              </h1>
              <p className="flex items-center space-x-2">
                <span className="text-2xl text-yellow-500">
                  <BsStarFill />
                </span>
                <span className="pt-1">{item.vote_average}</span>
              </p>
              <p className="leading-7">
                {truncateString(item.overview)}
              </p>
            </div>
          </div>
        </MaxWidthLayout>
      </div>

      <div className="player-container">
        <iframe
          src={videoSrc()}
          frameborder="0"
          allow="autoplay; encrypted-media"
          allowfullscreen
          title="video player"
          className="w-full h-96"
        ></iframe>
        <div className="server-buttons flex justify-center space-x-4 my-4">
          <button onClick={() => setServer(1)}>Server 1</button>
          <button onClick={() => setServer(2)}>Server 2</button>
          <button onClick={() => setServer(3)}>Server 3</button>
        </div>
        {mediaType === 'tv' && (
          <div className="season-episode-selector flex justify-center space-x-4 my-4">
            <input
              type="number"
              value={season}
              onChange={(e) => setSeason(e.target.value)}
              placeholder="Season"
              className="season-input"
            />
            <input
              type="number"
              value={episode}
              onChange={(e) => setEpisode(e.target.value)}
              placeholder="Episode"
              className="episode-input"
            />
          </div>
        )}
      </div>

      <TopSection>
        <MaxWidthLayout>
          <div className="grid grid-cols-1 md:grid-cols-[76%_20%] gap-[4%]">
            <div className="space-y-8">
              <h2 className="custom-section-title">All Casts</h2>
              <div className="grid grid-cols-2 gap-8 sm:grid-cols-3 md:grid-cols-4">
                {credits.cast.slice(0, 16).map((singleCast) => (
                  <div key={singleCast.id} className="space-y-2">
                    <div className="max-h-[320px] overflow-hidden rounded-md">
                      {singleCast.profile_path ? (
                        <img
                          src={extractImgPoster(singleCast.profile_path)}
                          className="object-cover w-full h-full rounded-md"
                        />
                      ) : (
                        <div className="h-[320px] rounded-md bg-gray-100"></div>
                      )}
                    </div>
                    <h2>{singleCast.original_name}</h2>
                    <p>{singleCast.character}</p>
                  </div>
                ))}
              </div>
            </div>
            <div className="space-y-8">
              {/* Additional details */}
              {/* Similar to the existing ones */}
            </div>
          </div>
        </MaxWidthLayout>
      </TopSection>
      <OtherSection>
        {similarItems && (
          <MovieContainer
            sectionTitle="Similar Movies/TV Shows"
            moviesList={similarItems}
          />
        )}
      </OtherSection>
    </NavbarFooterIncluded>
  );
};

export default SingleMovie;
