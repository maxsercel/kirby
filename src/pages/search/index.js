import React, { useEffect, useState } from "react";
import { MaxWidthLayout, NavbarFooterIncluded, TopSection } from "layouts";
import { Pagination, MovieCard } from "components";
import { getSearchedMovieAndTV } from "services/api"; // Updated function
import { useSearchProvider } from "contexts/searchContext";

const SearchMovie = () => {
  const [results, setResults] = useState();
  const [selectedPage, setSelectedPage] = useState(1);
  const { searchedKey } = useSearchProvider();
  const moviesPerPage = 20;
  const numberOfRecordsVisited = (selectedPage - 1) * moviesPerPage;
  
  useEffect(() => {
    (async function () {
      const {
        results: searchResults,
        total_pages,
        total_results,
      } = await getSearchedMovieAndTV(searchedKey, selectedPage);
      setResults({ searchResults, total_pages, total_results });
    })();
  }, [selectedPage, searchedKey]);

  const totalPagesCalculated = Math.ceil(
    results?.total_results / moviesPerPage
  );

  return (
    <NavbarFooterIncluded>
      <MaxWidthLayout>
        <TopSection>
          <div className="flex flex-col space-y-5 md:space-y-0 md:flex-row md:items-center md:space-x-5 md:justify-between">
            <h2 className="text-3xl uppercase font-AtypDisplayBold">
              {`Searched : ${searchedKey}`}
            </h2>
          </div>
          <div className="grid grid-cols-2 gap-8 md:grid-cols-4 lg:grid-cols-5 md:gap-10">
            {results?.searchResults
              .slice(
                numberOfRecordsVisited,
                numberOfRecordsVisited + moviesPerPage
              )
              .map((item) => (
                <MovieCard key={item.id} item={item} />
              ))}
          </div>
          <div>
            <Pagination
              totalPagesCalculated={totalPagesCalculated}
              handlePageChange={setSelectedPage}
            />
          </div>
        </TopSection>
      </MaxWidthLayout>
    </NavbarFooterIncluded>
  );
};

export default SearchMovie;
