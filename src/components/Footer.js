import React from "react";

const Footer = () => {
  return (
    <div className="p-5">
      <p className="text-center">
      This site does not store any files on its server. All contents are provided by non-affiliated third parties.
      </p>
    </div>
  );
};

export default Footer;
